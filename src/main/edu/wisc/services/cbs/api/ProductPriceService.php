<?php

namespace edu\wisc\services\cbs\api;

use Money\Money;

/**
 * Service intended for managing product prices.
 */
interface ProductPriceService
{

    /**
     * Update the price for the given SKU.
     * 
     * @param $sku
     * @param Money $price
     * @return ProductPriceServiceResponse
     */
    public function updateProductPrice($sku, Money $price);

}

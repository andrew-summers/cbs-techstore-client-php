<?php

namespace edu\wisc\services\cbs\api;

/**
 * Abstract base class defining common service response functions.
 */
abstract class AbstractServiceResponse
{

    /** @var boolean */
    private $success;

    /** @var string */
    private $message;

    /**
     * ProductServiceResponse constructor.
     * @param bool $success
     * @param string $message
     */
    public function __construct($success, $message)
    {
        $this->success = $success;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}
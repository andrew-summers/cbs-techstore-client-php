<?php

namespace edu\wisc\services\cbs\api;

/**
 * The status of a call to a {@link ProductPriceService} call.
 */
class ProductPriceServiceResponse extends AbstractServiceResponse
{

}

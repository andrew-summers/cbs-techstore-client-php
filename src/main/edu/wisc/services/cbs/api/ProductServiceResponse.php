<?php
namespace edu\wisc\services\cbs\api;

/**
 * The status of a call to a {@link ProductService} method.
 */
class ProductServiceResponse extends AbstractServiceResponse
{
    
}

<?php

namespace edu\wisc\services\cbs\api;

/**
 * Service interface for managing products.
 *
 * @author Nicholas Blair
 */
interface ProductService
{

    /**
     * Create a new product.
     *
     * @param Product product to create
     * @return ProductServiceResponse
     */
    public function createProduct(Product $product);

    /**
     * Update an existing product.
     *
     * @param Product product to update
     * @return ProductServiceResponse
     */
    public function updateProduct(Product $product);

}

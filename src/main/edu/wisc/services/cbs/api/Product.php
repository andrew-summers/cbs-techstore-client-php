<?php

namespace edu\wisc\services\cbs\api;

use Money\Money;

/**
 * Intermediate representation of a "Product", independent of system (e.g. Magento, CBS).
 *
 * @author Nicholas Blair
 */
class Product
{
    /** @var string  Stock Keeping Unit, a unique product identifier */
    private $sku = '';

    /** @var string  name / short description */
    private $name = '';

    /** @var Money  cost */
    private $cost;

    /** @var string */
    private $category = '';

    /** @var string */
    private $manufacturer = '';

    /** @var string */
    private $manufacturerPartNumber = '';

    /** @var float  minimum quantity allowed in inventory */
    private $minQuantity;

    /** @var float  maximum quantity allowed in inventory */
    private $maxQuantity;
    
    /** @var string */
    private $pricingTemplate;

    /** @var string  state of the item within the Techstore, e.g. 'FULLY_SELLABLE' or 'FROZEN'*/
    private $lifecycle = '';

    /** @var string  control flag for capturing serial numbers */
    private $serialControlFlag = '';

    /** @var string  product vendor */
    private $vendor = '';

    /** @var string  vendor part number */
    private $vendorPartNumber = '';

    /** @var string  UPC (barcode) */
    private $upc = '';

    /** @var int  CBS Item Template */
    private $cbsItemTemplate;

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }
    /**
     * @param string $sku
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Money
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param Money $cost
     * @return Product
     */
    public function setCost(Money $cost = null)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     * @return Product
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturerPartNumber()
    {
        return $this->manufacturerPartNumber;
    }

    /**
     * @param string $manufacturerPartNumber
     * @return Product
     */
    public function setManufacturerPartNumber($manufacturerPartNumber)
    {
        $this->manufacturerPartNumber = $manufacturerPartNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getMinQuantity()
    {
        return $this->minQuantity;
    }

    /**
     * @param string $minQuantity
     * @return Product
     */
    public function setMinQuantity($minQuantity)
    {
        $this->minQuantity = $minQuantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * @param string $maxQuantity
     * @return Product
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getLifecycle()
    {
        return $this->lifecycle;
    }

    /**
     * @param string $lifecycle
     * @return Product
     */
    public function setLifecycle($lifecycle)
    {
        $this->lifecycle = $lifecycle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerialControlFlag()
    {
        return $this->serialControlFlag;
    }

    /**
     * @param string $serialControlFlag
     * @return Product
     */
    public function setSerialControlFlag($serialControlFlag)
    {
        $this->serialControlFlag = $serialControlFlag;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     * @return Product
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorPartNumber()
    {
        return $this->vendorPartNumber;
    }

    /**
     * @param string $vendorPartNumber
     * @return Product
     */
    public function setVendorPartNumber($vendorPartNumber)
    {
        $this->vendorPartNumber = $vendorPartNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param string $upc
     * @return Product
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
        return $this;
    }

    /**
     * @return int
     */
    public function getCbsItemTemplate()
    {
        return $this->cbsItemTemplate;
    }

    /**
     * @param int $cbsItemTemplate
     * @return Product
     */
    public function setCbsItemTemplate($cbsItemTemplate)
    {
        $this->cbsItemTemplate = $cbsItemTemplate;
        return $this;
    }

    /**
     * @param string $pricingTemplate
     * @return Product
     */
    public function setPricingTemplate($pricingTemplate)
    {
        $this->pricingTemplate = $pricingTemplate;
        return $this;
    }

    /**
     * @return string
     */
    public function getPricingTemplate()
    {
        return $this->pricingTemplate;
    }

}

<?php


 function autoload_cb3949d7e849cc1ac262a6b1ded3707b($class)
{
    $classes = array(
        'edu\wisc\services\cbs\wsdlGeneratedClasses\DOIT_SOA_ITEM_V2_Service' => __DIR__ .'/DOIT_SOA_ITEM_V2_Service.php',
        'edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\wsdlGeneratedClasses\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\wsdlGeneratedClasses\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_cb3949d7e849cc1ac262a6b1ded3707b');

// Do nothing. The rest is just leftovers from the code generation.
{
}

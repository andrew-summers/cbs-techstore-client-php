<?php

namespace edu\wisc\services\cbs\wsdlGeneratedClasses;

class DOIT_SOA_ITEM_V2_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\wsdlGeneratedClasses\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\wsdlGeneratedClasses\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\wsdlGeneratedClasses\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = 'src/main/resources/doit_soa_item.xml')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ITEM_CREATE(InputParameters $body)
    {
      return $this->__soapCall('ITEM_CREATE', array($body));
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ITEM_UPDATE(InputParameters $body)
    {
      return $this->__soapCall('ITEM_UPDATE', array($body));
    }

}

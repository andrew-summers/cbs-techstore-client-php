<?php

namespace edu\wisc\services\cbs\wsdlGeneratedClasses;

class InputParameters
{

    /**
     * @var string $P_ITEM_NUMBER
     */
    protected $P_ITEM_NUMBER = null;

    /**
     * @var string $P_ITEM_DESCRIPTION
     */
    protected $P_ITEM_DESCRIPTION = null;

    /**
     * @var float $P_CBS_ITEM_TEMPLATE
     */
    protected $P_CBS_ITEM_TEMPLATE = null;

    /**
     * @var string $P_ITEM_CATEGORY
     */
    protected $P_ITEM_CATEGORY = null;

    /**
     * @var string $P_MANUFACTURER_NAME
     */
    protected $P_MANUFACTURER_NAME = null;

    /**
     * @var string $P_MANUFACTURER_PART_NUMBER
     */
    protected $P_MANUFACTURER_PART_NUMBER = null;

    /**
     * @var float $P_COST
     */
    protected $P_COST = null;

    /**
     * @var float $P_MINMAX_LEVEL_MIN
     */
    protected $P_MINMAX_LEVEL_MIN = null;

    /**
     * @var float $P_MINMAX_LEVEL_MAX
     */
    protected $P_MINMAX_LEVEL_MAX = null;

    /**
     * @var string $P_LIFE_CYCLE
     */
    protected $P_LIFE_CYCLE = null;

    /**
     * @var string $P_SERIAL_CONTROL_FLAG
     */
    protected $P_SERIAL_CONTROL_FLAG = null;

    /**
     * @var string $P_VENDOR
     */
    protected $P_VENDOR = null;

    /**
     * @var string $P_SUPPLIER_PART_NUMBER
     */
    protected $P_SUPPLIER_PART_NUMBER = null;

    /**
     * @var string $P_UPC
     */
    protected $P_UPC = null;

    /**
     * @var float $P_BASE_PRICE
     */
    protected $P_BASE_PRICE = null;

    /**
     * @var float $P_PRICING_TEMPLATE
     */
    protected $P_PRICING_TEMPLATE = null;

    /**
     * @var string $P_REFERENCE
     */
    protected $P_REFERENCE = null;

    /**
     * @param string $P_ITEM_NUMBER
     * @param string $P_ITEM_DESCRIPTION
     * @param float $P_CBS_ITEM_TEMPLATE
     * @param string $P_ITEM_CATEGORY
     * @param string $P_MANUFACTURER_NAME
     * @param string $P_MANUFACTURER_PART_NUMBER
     * @param float $P_COST
     * @param float $P_MINMAX_LEVEL_MIN
     * @param float $P_MINMAX_LEVEL_MAX
     * @param string $P_LIFE_CYCLE
     * @param string $P_SERIAL_CONTROL_FLAG
     * @param string $P_VENDOR
     * @param string $P_SUPPLIER_PART_NUMBER
     * @param string $P_UPC
     * @param float $P_BASE_PRICE
     * @param float $P_PRICING_TEMPLATE
     * @param string $P_REFERENCE
     */
    public function __construct($P_ITEM_NUMBER, $P_ITEM_DESCRIPTION, $P_CBS_ITEM_TEMPLATE, $P_ITEM_CATEGORY, $P_MANUFACTURER_NAME, $P_MANUFACTURER_PART_NUMBER, $P_COST, $P_MINMAX_LEVEL_MIN, $P_MINMAX_LEVEL_MAX, $P_LIFE_CYCLE, $P_SERIAL_CONTROL_FLAG, $P_VENDOR, $P_SUPPLIER_PART_NUMBER, $P_UPC, $P_BASE_PRICE, $P_PRICING_TEMPLATE, $P_REFERENCE)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      $this->P_ITEM_DESCRIPTION = $P_ITEM_DESCRIPTION;
      $this->P_CBS_ITEM_TEMPLATE = $P_CBS_ITEM_TEMPLATE;
      $this->P_ITEM_CATEGORY = $P_ITEM_CATEGORY;
      $this->P_MANUFACTURER_NAME = $P_MANUFACTURER_NAME;
      $this->P_MANUFACTURER_PART_NUMBER = $P_MANUFACTURER_PART_NUMBER;
      $this->P_COST = $P_COST;
      $this->P_MINMAX_LEVEL_MIN = $P_MINMAX_LEVEL_MIN;
      $this->P_MINMAX_LEVEL_MAX = $P_MINMAX_LEVEL_MAX;
      $this->P_LIFE_CYCLE = $P_LIFE_CYCLE;
      $this->P_SERIAL_CONTROL_FLAG = $P_SERIAL_CONTROL_FLAG;
      $this->P_VENDOR = $P_VENDOR;
      $this->P_SUPPLIER_PART_NUMBER = $P_SUPPLIER_PART_NUMBER;
      $this->P_UPC = $P_UPC;
      $this->P_BASE_PRICE = $P_BASE_PRICE;
      $this->P_PRICING_TEMPLATE = $P_PRICING_TEMPLATE;
      $this->P_REFERENCE = $P_REFERENCE;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NUMBER()
    {
      return $this->P_ITEM_NUMBER;
    }

    /**
     * @param string $P_ITEM_NUMBER
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_ITEM_NUMBER($P_ITEM_NUMBER)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_DESCRIPTION()
    {
      return $this->P_ITEM_DESCRIPTION;
    }

    /**
     * @param string $P_ITEM_DESCRIPTION
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_ITEM_DESCRIPTION($P_ITEM_DESCRIPTION)
    {
      $this->P_ITEM_DESCRIPTION = $P_ITEM_DESCRIPTION;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_CBS_ITEM_TEMPLATE()
    {
      return $this->P_CBS_ITEM_TEMPLATE;
    }

    /**
     * @param float $P_CBS_ITEM_TEMPLATE
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_CBS_ITEM_TEMPLATE($P_CBS_ITEM_TEMPLATE)
    {
      $this->P_CBS_ITEM_TEMPLATE = $P_CBS_ITEM_TEMPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_CATEGORY()
    {
      return $this->P_ITEM_CATEGORY;
    }

    /**
     * @param string $P_ITEM_CATEGORY
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_ITEM_CATEGORY($P_ITEM_CATEGORY)
    {
      $this->P_ITEM_CATEGORY = $P_ITEM_CATEGORY;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_MANUFACTURER_NAME()
    {
      return $this->P_MANUFACTURER_NAME;
    }

    /**
     * @param string $P_MANUFACTURER_NAME
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_MANUFACTURER_NAME($P_MANUFACTURER_NAME)
    {
      $this->P_MANUFACTURER_NAME = $P_MANUFACTURER_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_MANUFACTURER_PART_NUMBER()
    {
      return $this->P_MANUFACTURER_PART_NUMBER;
    }

    /**
     * @param string $P_MANUFACTURER_PART_NUMBER
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_MANUFACTURER_PART_NUMBER($P_MANUFACTURER_PART_NUMBER)
    {
      $this->P_MANUFACTURER_PART_NUMBER = $P_MANUFACTURER_PART_NUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_COST()
    {
      return $this->P_COST;
    }

    /**
     * @param float $P_COST
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_COST($P_COST)
    {
      $this->P_COST = $P_COST;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_MINMAX_LEVEL_MIN()
    {
      return $this->P_MINMAX_LEVEL_MIN;
    }

    /**
     * @param float $P_MINMAX_LEVEL_MIN
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_MINMAX_LEVEL_MIN($P_MINMAX_LEVEL_MIN)
    {
      $this->P_MINMAX_LEVEL_MIN = $P_MINMAX_LEVEL_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_MINMAX_LEVEL_MAX()
    {
      return $this->P_MINMAX_LEVEL_MAX;
    }

    /**
     * @param float $P_MINMAX_LEVEL_MAX
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_MINMAX_LEVEL_MAX($P_MINMAX_LEVEL_MAX)
    {
      $this->P_MINMAX_LEVEL_MAX = $P_MINMAX_LEVEL_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_LIFE_CYCLE()
    {
      return $this->P_LIFE_CYCLE;
    }

    /**
     * @param string $P_LIFE_CYCLE
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_LIFE_CYCLE($P_LIFE_CYCLE)
    {
      $this->P_LIFE_CYCLE = $P_LIFE_CYCLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SERIAL_CONTROL_FLAG()
    {
      return $this->P_SERIAL_CONTROL_FLAG;
    }

    /**
     * @param string $P_SERIAL_CONTROL_FLAG
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_SERIAL_CONTROL_FLAG($P_SERIAL_CONTROL_FLAG)
    {
      $this->P_SERIAL_CONTROL_FLAG = $P_SERIAL_CONTROL_FLAG;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_VENDOR()
    {
      return $this->P_VENDOR;
    }

    /**
     * @param string $P_VENDOR
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_VENDOR($P_VENDOR)
    {
      $this->P_VENDOR = $P_VENDOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SUPPLIER_PART_NUMBER()
    {
      return $this->P_SUPPLIER_PART_NUMBER;
    }

    /**
     * @param string $P_SUPPLIER_PART_NUMBER
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_SUPPLIER_PART_NUMBER($P_SUPPLIER_PART_NUMBER)
    {
      $this->P_SUPPLIER_PART_NUMBER = $P_SUPPLIER_PART_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_UPC()
    {
      return $this->P_UPC;
    }

    /**
     * @param string $P_UPC
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_UPC($P_UPC)
    {
      $this->P_UPC = $P_UPC;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_BASE_PRICE()
    {
      return $this->P_BASE_PRICE;
    }

    /**
     * @param float $P_BASE_PRICE
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_BASE_PRICE($P_BASE_PRICE)
    {
      $this->P_BASE_PRICE = $P_BASE_PRICE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_PRICING_TEMPLATE()
    {
      return $this->P_PRICING_TEMPLATE;
    }

    /**
     * @param float $P_PRICING_TEMPLATE
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_PRICING_TEMPLATE($P_PRICING_TEMPLATE)
    {
      $this->P_PRICING_TEMPLATE = $P_PRICING_TEMPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_REFERENCE()
    {
      return $this->P_REFERENCE;
    }

    /**
     * @param string $P_REFERENCE
     * @return \edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters
     */
    public function setP_REFERENCE($P_REFERENCE)
    {
      $this->P_REFERENCE = $P_REFERENCE;
      return $this;
    }

}

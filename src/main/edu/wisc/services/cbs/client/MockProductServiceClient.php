<?php

namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\api\ProductService;
use edu\wisc\services\cbs\api\ProductServiceResponse;

class MockProductServiceClient implements ProductService
{

    /** @var ProductServiceResponse */
    private $response;

    /**
     * @param ProductServiceResponse $response  response that is sent back with each call
     */
    public function __construct(ProductServiceResponse $response = null)
    {
        if (null === $response) {
            $this->response = new ProductServiceResponse(true, 'Mock CBS Response');
            return;
        }
        $this->response = $response;
    }


    /**
     * {@inheritDoc}
     */
    public function createProduct(Product $product)
    {
        return $this->response;
    }

    /**
     * {@inheritDoc}
     */
    public function updateProduct(Product $product)
    {
        return $this->response;
    }

}

<?php
namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\wsdlGeneratedClasses\InputParameters;
use Money\Money;

/**
 * Maps a {@link Product} to an {@link InputParameters}
 */
class ProductInputParametersMapper
{

    /**
     * Maps a {@link Product} to an {@link InputParameters} instance
     * @param Product $product
     * @return InputParameters
     * @throws UnsupportedCurrencyException  when a currency other than USD is used in the Product
     */
    public static function toInputParameters(Product $product)
    {
        return (new InputParameters(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ))
            ->setP_CBS_ITEM_TEMPLATE($product->getCbsItemTemplate())
            ->setP_COST(static::convertMoneyToFloat($product->getCost()))
            ->setP_ITEM_CATEGORY($product->getCategory())
            ->setP_ITEM_DESCRIPTION($product->getName())
            ->setP_ITEM_NUMBER($product->getSku())
            ->setP_LIFE_CYCLE($product->getLifecycle())
            ->setP_MANUFACTURER_NAME($product->getManufacturer())
            ->setP_MANUFACTURER_PART_NUMBER($product->getManufacturerPartNumber())
            ->setP_MINMAX_LEVEL_MAX($product->getMaxQuantity())
            ->setP_MINMAX_LEVEL_MIN($product->getMinQuantity())
            ->setP_PRICING_TEMPLATE($product->getPricingTemplate())
            ->setP_SERIAL_CONTROL_FLAG($product->getSerialControlFlag())
            ->setP_SUPPLIER_PART_NUMBER($product->getVendorPartNumber())
            ->setP_UPC($product->getUpc())
            ->setP_VENDOR($product->getVendor());
    }

    /**
     * Converts a {@link Money} instance into a float
     * @param Money $money  money to convert
     * @return float|null  float value of money, null if null was provided
     * @throws UnsupportedCurrencyException  if a currency other than USD is provided
     */
    private static function convertMoneyToFloat(Money $money = null)
    {
        $costFloat = null;
        if (null !== $money) {
            $currency = $money->getCurrency()->getName();
            if ($currency !== 'USD') {
                throw new UnsupportedCurrencyException("Unsupported currency: $currency");
            }
            $amount = $money->getAmount();
            $costFloat = (float) $amount / 100;
        }
        return $costFloat;
    }

}

<?php

namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\ProductPriceService;
use edu\wisc\services\cbs\api\ProductPriceServiceResponse;
use Money\Money;

class MockProductPriceServiceClient implements ProductPriceService
{

    /** @var  ProductPriceServiceResponse */
    private $response;
    
    public function __construct(ProductPriceServiceResponse $response = null)
    {
        if ($response === null) {
            $this->response = new ProductPriceServiceResponse(true, 'Mock CBS Product Price Response');
            return;
        }
        $this->response = $response;
    }

    /**
     * {@inheritdoc}
     */
    public function updateProductPrice($sku, Money $price)
    {
        return $this->response;
    }
    
}

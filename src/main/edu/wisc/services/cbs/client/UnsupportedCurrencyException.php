<?php
namespace edu\wisc\services\cbs\client;

/**
 * Exception class for when an unsupported currency is encountered
 */
class UnsupportedCurrencyException extends \Exception
{
}

<?php
namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\api\ProductService;
use edu\wisc\services\cbs\api\ProductServiceResponse;
use edu\wisc\services\cbs\wsdlGeneratedClasses\DOIT_SOA_ITEM_V2_Service;
use edu\wisc\services\cbs\wsdlGeneratedClasses\OutputParameters;

/**
 * Class SoapTechstoreClient uses SOAP to create a product in the CBS database.
 */
class ProductServiceSoapClient implements ProductService
{

    /** @var DOIT_SOA_ITEM_V2_Service * */
    private $itemSoapClient;

    /**
     * SoapTechstoreClient constructor.
     * @param string $username
     * @param string $password
     * @param \SoapClient $itemSoapClient  override the SOAP client for the item service. Username and password ignored.
     */
    public function __construct($username, $password, \SoapClient $itemSoapClient = null)
    {
        if (null !== $itemSoapClient) {
            $this->itemSoapClient = $itemSoapClient;
            return;
        }
        $this->itemSoapClient = new DOIT_SOA_ITEM_V2_Service(
            [],
            __DIR__.'/../../../../../resources/doit_soa_item.xml'
        );
        $namespace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $token = new \stdClass;
        $token->Username = new \SoapVar($username, XSD_STRING, null, null, null, $namespace);
        $token->Password = new \SoapVar($password, XSD_STRING, null, null, null, $namespace);
        $wsec = new \stdClass;
        $wsec->UsernameToken = new \SoapVar($token, SOAP_ENC_OBJECT, null, null, null, $namespace);
        $headers = new \SOAPHeader($namespace, 'Security', $wsec, true);
        $this->itemSoapClient->__setSoapHeaders($headers);
    }

    /**
     * {@inheritDoc}
     */
    public function createProduct(Product $product)
    {
        $outputParameters = $this->itemSoapClient->ITEM_CREATE(
            ProductInputParametersMapper::toInputParameters($product)
        );
        return static::createResponse($outputParameters);
    }

    /**
     * {@inheritDoc}
     */
    public function updateProduct(Product $product)
    {
        $outputParameters = $this->itemSoapClient->ITEM_UPDATE(
            ProductInputParametersMapper::toInputParameters($product)
        );
        return static::createResponse($outputParameters);
    }

    /**
     * @param OutputParameters $outputParameters
     * @return ProductServiceResponse
     */
    public static function createResponse(OutputParameters $outputParameters)
    {
        return new ProductServiceResponse(
            strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') == 0,
            $outputParameters->getP_RESULT_MESSAGE()
        );
    }

}

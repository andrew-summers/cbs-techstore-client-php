<?php
namespace edu\wisc\services\cbs;

/**
 * A base test class for integration tests.
 *
 * <p>Loads the integration test data located in `src/test/resources/it-configuration.ini`</p>
 */
class IntegrationTestCase extends \PHPUnit_Framework_TestCase
{

    /** @var array  integration test data */
    protected static $itData;

    /**
     * Reads the integration test data
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        $itDataPath = __DIR__ . '/../../../../resources/it-configuration.ini';
        static::$itData = parse_ini_file($itDataPath);
        if (static::$itData === false) {
            static::fail("Unable to read integration test data from $itDataPath");
        }
    }
}

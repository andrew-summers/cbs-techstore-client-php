<?php

namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\ProductPriceServiceResponse;
use Money\Money;

class MockProductPriceServiceClientTest extends \PHPUnit_Framework_TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockProductPriceServiceClient();
        $response = $client->updateProductPrice("skusku", Money::USD(4200));
        static::assertTrue($response->isSuccess());
    }
    
    /** @test */
    public function returnsCustomResponse()
    {
        $mockResponse = new ProductPriceServiceResponse(true, "Custom Product Price Service Response");
        $client = new MockProductPriceServiceClient($mockResponse);
        $response = $client->updateProductPrice("skusku", Money::USD(4200));
        static::assertEquals($mockResponse, $response);
    }
    
}


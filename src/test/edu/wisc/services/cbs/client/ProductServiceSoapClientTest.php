<?php
namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\wsdlGeneratedClasses\DOIT_SOA_ITEM_V2_Service;
use edu\wisc\services\cbs\wsdlGeneratedClasses\OutputParameters;
use Mockery\MockInterface;

/**
 * Unit tests for {@link ProductServiceSoapClient}
 */
class ProductServiceSoapClientTest extends \PHPUnit_Framework_TestCase
{

    /** @var MockInterface */
    private $itemSoapService;

    /** @var ProductServiceSoapClient */
    private $productService;

    /**
     * Initializes the mock client before each test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->itemSoapService = \Mockery::mock(DOIT_SOA_ITEM_V2_Service::class);
        $this->productService = new ProductServiceSoapClient('', '', $this->itemSoapService);
    }

    /** @test */
    public function successResponseIsCaseInsensitive()
    {
        foreach (['success', 'SUCCESS', 'Success'] as $status) {
            static::assertTrue(
                ProductServiceSoapClient::createResponse(new OutputParameters($status, '', '', ''))->isSuccess()
            );
        }
    }

    /** @test */
    public function unsuccessfulByDefault()
    {
        foreach ([null, '', 0, 'NOPE'] as $status) {
            static::assertFalse(
                ProductServiceSoapClient::createResponse(new OutputParameters($status, '', '', ''))->isSuccess()
            );
        }
    }

}

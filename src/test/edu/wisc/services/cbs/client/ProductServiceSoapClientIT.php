<?php

namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\IntegrationTestCase;
use Money\Money;

/**
 * Integration tests for {@link ProductServiceSoapClient}
 *
 * @author Andrew Lundholm
 */
class ProductServiceSoapClientIT extends IntegrationTestCase
{

    /** @var  ProductServiceSoapClient */
    var $client;

    public function setUp() {
        $this->client = new ProductServiceSoapClient(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
    }

    public function testCreateProduct()
    {
        $response = $this->client->createProduct($this->generateSampleProduct());
        static::assertTrue($response->isSuccess());
    }

    public function testUpdateProduct()
    {
        // First create the product so that it exists in the first place
        $product = $this->generateSampleProduct();
        $response = $this->client->createProduct($product);
        static::assertTrue($response->isSuccess(), $response->getMessage());

        // Then update it
        $product->setCost(Money::USD(100));
        $response = $this->client->updateProduct($product);
        static::assertTrue($response->isSuccess(), $response->getMessage());
    }

    private function generateSampleProduct() {
        $itemNumber = mt_rand(0, 99999);
        $cost = mt_rand(1, 10000);
        return (new Product())
            ->setCategory('ADI IA Test Category')
            ->setCbsItemTemplate(1390)
            ->setCost(Money::USD($cost))
            ->setLifecycle('FULLY_SELLABLE')
            ->setManufacturer('Hoffmann Manufacturers')
            ->setManufacturerPartNumber("HOFF$itemNumber")
            ->setMaxQuantity(5)
            ->setMinQuantity(1)
            ->setName('ADI IA Test Product')
            ->setPricingTemplate(1)
            ->setSerialControlFlag('1')
            ->setSku("ADI-test-$itemNumber")
            ->setUpc(uniqid('', false))
            ->setVendor('Blair Wholesale Goods')
            ->setVendorPartNumber("BLAIR$itemNumber")
        ;
    }

}

<?php
namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use Money\Money;

/**
 * Unit tests for {@link ProductInputParametersMapper}
 */
class ProductInputParametersMapperTest extends \PHPUnit_Framework_TestCase
{

    /** @test */
    public function mapsProductToInputParameters()
    {
        $product = (new Product())
            ->setSku('itemNumber')
            ->setName('name')
            ->setCategory('category')
            ->setManufacturer('manufacturer')
            ->setManufacturerPartNumber('manufacturerPartNumber')
            ->setCost(Money::USD(99))
            ->setMinQuantity(1)
            ->setMaxQuantity(5)
            ->setLifecycle('lifecycle')
            ->setSerialControlFlag('serialCtrlFlag')
            ->setVendor('vendor')
            ->setVendorPartNumber('vendorPartNumber')
            ->setUpc('upc');

        $inputParameters = ProductInputParametersMapper::toInputParameters($product);

        static::assertEquals($product->getSku(), $inputParameters->getP_ITEM_NUMBER());
        static::assertEquals($product->getName(), $inputParameters->getP_ITEM_DESCRIPTION());
        static::assertEquals($product->getCategory(), $inputParameters->getP_ITEM_CATEGORY());
        static::assertEquals($product->getManufacturer(), $inputParameters->getP_MANUFACTURER_NAME());
        static::assertEquals($product->getManufacturerPartNumber(), $inputParameters->getP_MANUFACTURER_PART_NUMBER());
        static::assertTrue(
            $product->getCost()->equals(Money::USD(Money::stringToUnits($inputParameters->getP_COST())))
        );
        static::assertEquals($product->getMinQuantity(), $inputParameters->getP_MINMAX_LEVEL_MIN());
        static::assertEquals($product->getMaxQuantity(), $inputParameters->getP_MINMAX_LEVEL_MAX());
        static::assertEquals($product->getLifecycle(), $inputParameters->getP_LIFE_CYCLE());
        static::assertEquals($product->getSerialControlFlag(), $inputParameters->getP_SERIAL_CONTROL_FLAG());
        static::assertEquals($product->getVendor(), $inputParameters->getP_VENDOR());
        static::assertEquals($product->getVendorPartNumber(), $inputParameters->getP_SUPPLIER_PART_NUMBER());
        static::assertEquals($product->getUpc(), $inputParameters->getP_UPC());
    }

    /** @test */
    public function handlesNullCost()
    {
        $inputParameters = ProductInputParametersMapper::toInputParameters(new Product());
        static::assertNull($inputParameters->getP_COST());
    }

    /**
     * @test
     * @expectedException \edu\wisc\services\cbs\client\UnsupportedCurrencyException
     */
    public function throwsWhenNotUsd()
    {
        ProductInputParametersMapper::toInputParameters(
            (new Product())->setCost(
                Money::EUR(99)
            )
        );
    }
}

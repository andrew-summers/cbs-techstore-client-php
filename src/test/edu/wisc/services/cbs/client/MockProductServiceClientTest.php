<?php
namespace edu\wisc\services\cbs\client;

use edu\wisc\services\cbs\api\Product;
use edu\wisc\services\cbs\api\ProductServiceResponse;

/**
 * Tests for {@link MockProductServiceClient}
 */
class MockProductServiceClientTest extends \PHPUnit_Framework_TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockProductServiceClient();
        $response = $client->createProduct(new Product());
        static::assertTrue($response->isSuccess());
    }

    /** @test */
    public function returnsCustomResponse()
    {
        $mockResponse = new ProductServiceResponse(false, 'Product was not created');
        $client = new MockProductServiceClient($mockResponse);
        $response = $client->createProduct(new Product());
        static::assertEquals($mockResponse, $response);
    }

}

<?php

if ($argc !== 1) {
    echo "Error: Invalid number of arguments.";
    exit(1);
}


require_once(__DIR__."/vendor/autoload.php");

$outputDir = __DIR__.'/src/main/edu/wisc/services/cbs/wsdlGeneratedClasses';

if (file_exists($outputDir)) {
    $files = glob($outputDir.'/*'); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file))
            unlink($file); // delete file
    }
    rmdir($outputDir);
}

$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'src/main/resources/doit_soa_item.xml',
        'outputDir' => $outputDir,
        'namespaceName' => 'edu\wisc\services\cbs\wsdlGeneratedClasses'
    ))
);

?>
